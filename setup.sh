#!/bin/bash

echo "---- Iniciando instalacao do ambiente de Desenvolvimento PHP ---"

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update

echo "--- Definindo Senha padrao para o MySQL e suas ferramentas ---"

DEFAULTPASS="vagrant"
sudo debconf-set-selections <<EOF
mysql-server	mysql-server/root_password password $DEFAULTPASS
mysql-server	mysql-server/root_password_again password $DEFAULTPASS
dbconfig-common	dbconfig-common/mysql/app-pass password $DEFAULTPASS
dbconfig-common	dbconfig-common/mysql/admin-pass password $DEFAULTPASS
dbconfig-common	dbconfig-common/password-confirm password $DEFAULTPASS
dbconfig-common	dbconfig-common/app-password-confirm password $DEFAULTPASS
phpmyadmin		phpmyadmin/reconfigure-webserver multiselect apache2
phpmyadmin		phpmyadmin/dbconfig-install boolean true
phpmyadmin      phpmyadmin/app-password-confirm password $DEFAULTPASS 
phpmyadmin      phpmyadmin/mysql/admin-pass     password $DEFAULTPASS
phpmyadmin      phpmyadmin/password-confirm     password $DEFAULTPASS
phpmyadmin      phpmyadmin/setup-password       password $DEFAULTPASS
phpmyadmin      phpmyadmin/mysql/app-pass       password $DEFAULTPASS
EOF

echo "--- Instalando pacotes basicos ---"
sudo apt-get install software-properties-common vim curl python-software-properties git-core --assume-yes --allow-downgrades --allow-remove-essential --allow-change-held-packages

echo "--- Adicionando repositorio do pacote PHP ---"
sudo add-apt-repository -y ppa:ondrej/php

echo "--- Atualizando lista de pacotes ---"
sudo apt-get update

echo "--- Instalando MySQL, Phpmyadmin e alguns outros modulos ---"
sudo apt-get install mysql-server-5.7 mysql-client phpmyadmin --assume-yes --allow-downgrades --allow-remove-essential --allow-change-held-packages

echo "--- Instalando PHP, Apache e alguns modulos ---"
sudo apt-get install php7.3 php7.3-cli php7.3-common --assume-yes --allow-downgrades --allow-remove-essential --allow-change-held-packages
sudo apt-get install php-pear php7.3-curl php7.3-dev php7.3-gd php7.3-mbstring php7.3-zip php7.3-mysql php7.3-xml php7.3-fpm libapache2-mod-php7.3 php7.3-imagick php7.3-recode php7.3-tidy php7.3-xmlrpc php7.3-intl zip unzip --assume-yes --allow-downgrades --allow-remove-essential --allow-change-held-packages

echo "--- Habilitando o PHP 7.3 ---"
sudo a2dismod php5
sudo a2dismod php7.0
sudo a2enmod php7.3

echo "--- Habilitando mod-rewrite do Apache ---"
sudo a2enmod rewrite

echo "--- Reiniciando Apache ---"
sudo service apache2 restart

echo "--- Criando um virtual hosts de exemplo, lembre-se de edita-lo quando entrar na vm e ativa-lo com sudo a2ensite allegiant.local.conf ---"
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/allegiant.local.conf
sudo mkdir -p /var/www/allegiant.local/public_html
sudo chown -R $USER:$USER /var/www/allegiant.local/public_html
sudo chmod -R 755 /var/www

echo "--- Baixando e Instalando Composer ---"
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

#echo "--- Instalando Banco NoSQL -> Redis <- ---" 
#sudo apt-get install redis-server --assume-yes
#sudo apt-get install php7.3-redis --assume-yes

# Instale apartir daqui o que você desejar 

echo "[OK] --- Ambiente de desenvolvimento concluido ---"